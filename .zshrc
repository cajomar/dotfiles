typeset -U path PATH

if [[ -d "$HOME/Android/Sdk" ]]; then
  export ANDROID_SDK_ROOT="$HOME/Android/Sdk"
  if [[ -d "$ANDROID_SDK_ROOT/ndk" ]]; then
    export ANDROID_NDK_ROOT=$(ls -d "$ANDROID_SDK_ROOT"/ndk/* | tail -1)
    path+=$ANDROID_NDK_ROOT
  fi
  path+="$ANDROID_SDK_ROOT/build-tools"
  path+="$ANDROID_SDK_ROOT/platform-tools"
  path+="$ANDROID_SDK_ROOT/tools"
fi

path=($HOME/.bin $HOME/.local/bin $HOME/.cargo/bin $path)


alias df='df -h'
# alias diff='diff --color --unified'
alias free='free -m'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias ip='ip --color=auto'
alias ls='ls --color=auto'
alias pysr='python3 -m http.server'
alias lg='lazygit'

autoload -U colors && colors
autoload -Uz compinit && compinit
autoload -Uz vcs_info

export EDITOR='nvim'
export PAGER='less'
export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=10000
export SAVEHIST=10000
export BROWSER='firefox'
export CMAKE_EXPORT_COMPILE_COMMANDS='ON'
# Force color output for GCC and Clang with Ninja
export CFLAGS='-fdiagnostics-color=always'
export CXXFLAGS='-fdiagnostics-color=always'
export CPPFLAGS='-fdiagnostics-color=always'
export RIPGREP_CONFIG_PATH="$HOME/.config/ripgreprc"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

setopt appendhistory
setopt histallowclobber
setopt histignorealldups
setopt histignoredups
setopt histsavenodups
setopt nonomatch
setopt promptsubst

_comp_options+=(globdots)

# Load version control information
zstyle ':vcs_info:*' enable git svn hg
precmd() { vcs_info }

# Format the vcs_info_msg_0_ variable
zstyle ':vcs_info:git:*' formats '  %b%m%u%c'
 
PROMPT="%{%B%F{green}%}%n%{%F{white}%}@%{%F{blue}%}%m%{%F{white}%}:%{%F{yellow}%}%32<..<%~%<<%{%F{magenta}%}\${vcs_info_msg_0_}%{%(?.$fg[white].$fg[red])%} %%%{%b%f%} "

bindkey '^R' history-incremental-search-backward
bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward


[[ -f "$HOME/.cargo/env" ]] && . "$HOME/.cargo/env"
[[ -f "$HOME/.zshrc.local" ]] && . "$HOME/.zshrc.local"

# vim: ft=sh
