#!/bin/bash
pass $(fd . .password-store/ -e .gpg -tf -x echo {.} | grep -Po ".password-store/\K.*" | fuzzel --dmenu) | head -1 | wl-copy
wl-paste
