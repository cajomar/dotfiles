vim.cmd([[
" Switch between .c and .h files with :A
command A ClangdSwitchSourceHeader
command AS split|ClangdSwitchSourceHeader
command AV vsplit|ClangdSwitchSourceHeader
command AT tabnew|ClangdSwitchSourceHeader


" Use italics in terminal for all colorschemes
"au ColorScheme * hi Comment cterm=italic | hi String cterm=italic
" Set the pop background color for the xoria256 colorscheme
autocmd ColorScheme xoria256 highlight Pmenu     ctermfg=252 guifg=#d0d0d0 ctermbg=237 guibg=#3a3a3a |
                        highlight PmenuSel  ctermfg=237 guifg=#3a3a3a ctermbg=252 guibg=#d0d0d0

autocmd FileType gitrebase,gitconfig set bufhidden=delete
]])

local autocmd = vim.api.nvim_create_autocmd

autocmd('BufEnter', {
  pattern = {'github.com_*.txt'},
  command = "set filetype=markdown"
})

-- Jump to the last position when reopening a file
autocmd('BufReadPost', {
  pattern = {'*'},
  callback = function()
    local ft = vim.opt_local.filetype:get()
    -- don't apply to git messages
    if (ft:match('commit') or ft:match('rebase')) then
      return
    end
    -- get position of last saved edit
    local pos = vim.api.nvim_buf_get_mark(0,'"')
    -- if in range, go there
    if (pos[1] > 1) and (pos[1] <= vim.api.nvim_buf_line_count(0)) then
      vim.api.nvim_win_set_cursor(0, pos)
    end
  end
})

-- Highlight text when yanking
autocmd('TextYankPost', {
  pattern = {'*'},
  callback = function(ev)
    vim.highlight.on_yank{on_visual = false}
  end,
})


autocmd('BufWrite', {
  pattern = {
    '*.c',
    '*.cpp',
    '*.h',
    '*.hpp',
    '*.rs',
    '*.zig',
    '*.py',
    '*.js',
    '*.ts',
    '*.xml',
  },
  callback = function(ev)
    vim.lsp.buf.format{async=true}
  end,
})

vim.filetype.add {
  filename = {
    ['config.h.in'] = 'cmakeconfig',
  }
}

vim.filetype.add {
  extension = {
    glsl = 'glsl',
    frag = 'glsl',
    vert = 'glsl',
    goem = 'glsl',
    tesc = 'glsl',
    comp = 'glsl',
    fs = 'glsl',
    vs = 'glsl',
    gs = 'glsl',
  }
}

vim.filetype.add {
  extension = {
    ['tpl'] = 'tempita',
    ['in'] = 'tempita',
  }
}

vim.filetype.add {
  pattern = {
    [".*/hypr/.*%.conf"] = "hyprlang"
  }
}

vim.opt.wildignore:append{'*.pyc', '*.pyo', '*.swap', 'Cargo.lock'}

vim.opt.clipboard = {'unnamed', 'unnamedplus'}

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2

vim.opt.listchars = {
  tab = '▸·',
  -- eol = '↵',
  trail = '·',
  nbsp ='␣',
  multispace = ' ',
}
vim.opt.list = true
vim.opt.completeopt = {'menu', 'menuone', 'noselect'}

vim.opt.termguicolors = true
vim.opt.autowrite = true
vim.opt.cursorline = true
vim.opt.expandtab = true
vim.opt.hidden = true
vim.opt.ignorecase = true
vim.opt.scrolloff=2
vim.opt.showmatch = true
vim.opt.smartcase = true
vim.opt.termguicolors = true
vim.opt.title = true
vim.opt.mouse = 'a'
vim.opt.number = true

vim.o.titlestring = "%{fnamemodify(getcwd(),':~')}"

vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

vim.g.guifont = 'FiraCode Mono Nerd Font Mono:h15'

vim.g.gruvbox_italic = 1
vim.g.solarized_italics = 1
vim.g.jellybeans_use_term_italics = 1
vim.g.jellybeans_use_term_italics = 1

local lazypath = vim.fn.stdpath('data') .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

require('lazy').setup('plugins')

-- vim.lsp.set_log_level 'trace'

local opts = { noremap = true}

local function nmap(lhs, rhs) vim.keymap.set('n', lhs, rhs, opts) end
local function imap(lhs, rhs) vim.keymap.set('i', lhs, rhs, opts) end
local function omap(lhs, rhs) vim.keymap.set('o', lhs, rhs, opts) end
local function vmap(lhs, rhs) vim.keymap.set('v', lhs, rhs, opts) end
local function smap(lhs, rhs) vim.keymap.set('s', lhs, rhs, opts) end
local function tmap(lhs, rhs) vim.keymap.set('t', lhs, rhs, opts) end
local function map(mode, l, r) vim.keymap.set(mode, l, r, opts) end

imap('<c-n>', function() require'luasnip'.jump(1) end)
smap('<c-n>', function() require'luasnip'.jump(1) end)
imap('<c-p>', function() require'luasnip'.jump(-1) end)
smap('<c-p>', function() require'luasnip'.jump(-1) end)

omap('<leader>a', vim.lsp.buf.code_action)
nmap('<leader>a', vim.lsp.buf.code_action)
nmap('<leader>d', vim.diagnostic.open_float)
nmap('<leader>fc', function() vim.lsp.buf.format{async = true} end)

nmap('<leader>sC', '<cmd>Telescope colorscheme<cr>')
nmap('<leader>sb', '<cmd>Telescope buffers<cr>')
nmap('<leader>sdd', '<cmd>Telescope diagnostics<cr>')
nmap('<leader>sds', '<cmd>Telescope lsp_document_symbols<cr>')
nmap('<leader>sws', '<cmd>Telescope lsp_dynamic_workspace_symbols<cr>')
nmap('<leader>sf', '<cmd>Telescope find_files<cr>')
nmap('<leader>sg', '<cmd>Telescope live_grep<cr>')
nmap('<leader>sh', '<cmd>Telescope help_tags<cr>')
nmap('<leader>sy', '<cmd>Telescope registers<cr>')
nmap('<leader>sz', '<cmd>Telescope spell_suggest<cr>')
nmap('<leader>sp', '<cmd>Telescope projects<cr>')
nmap('<leader>gS', '<cmd>Telescope git_status<cr>')
nmap('<leader>gc', '<cmd>Telescope git_commits<cr>')
nmap('<leader>gs', '<cmd>Telescope git_stash<cr>')
nmap('<leader>p', '<cmd>Telescope projects<cr>')
nmap('gd', '<cmd>Telescope lsp_definitions<cr>')
nmap('gi', '<cmd>Telescope lsp_implementations<cr>')
nmap('gr', '<cmd>Telescope lsp_references<cr>')

nmap('<leader>bd', '<cmd>BufferLinePickClose<cr>')
nmap('<leader>e', '<cmd>BufferLinePick<cr>')
nmap('<leader>bh', 'cmd>BufferLineCyclePrev<cr>')
nmap('<leader>bl', 'cmd>BufferLineCycleNext<cr>')

nmap('<leader>gt', vim.lsp.buf.type_definition)
nmap('<leader>k', vim.lsp.buf.hover)
nmap('<leader>q', vim.diagnostic.setloclist)
nmap('<leader>rn', vim.lsp.buf.rename)
nmap('<leader>wa', vim.lsp.buf.add_workspace_folder)
nmap('<leader>wl<', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end)
nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder)
nmap('[d', vim.diagnostic.goto_prev)
nmap(']d', vim.diagnostic.goto_next)
nmap('gD', vim.lsp.buf.declaration)
nmap('gW', vim.lsp.buf.workspace_symbol)
nmap('gs', vim.lsp.buf.signature_help)
nmap('gw', vim.lsp.buf.document_symbol)

-- Remove all trailing whitespaces by pressing F5
nmap('<f5>', ':let _s=@/<bar>:%s/\\s\\+$//e<bar>:let @/=_s<bar><cr>')

-- Press ; to execute a shell command; exit with an insert command followed by
-- any other key (e.g. ii, aa, ij)
nmap('<space>;', ':vsplit term://')

-- Exit insert mode with `jk`
imap('jk', '<esc>')
tmap('jk', '<c-\\><c-n>')
tmap('<s-esc>', '<c-\\><c-n>')

-- Navigate windows with ALT-[hljk]
map({'i', 't'}, '<A-h>', '<C-\\><C-N><C-w>h')
map({'i', 't'}, '<A-j>', '<C-\\><C-N><C-w>j')
map({'i', 't'}, '<A-k>', '<C-\\><C-N><C-w>k')
map({'i', 't'}, '<A-l>', '<C-\\><C-N><C-w>l')
nmap('<A-h>', '<C-w>h')
nmap('<A-j>', '<C-w>j')
nmap('<A-k>', '<C-w>k')
nmap('<A-l>', '<C-w>l')

map({'i', 't'}, '˙', '<C-\\><C-N><C-w>h')
map({'i', 't'}, '∆', '<C-\\><C-N><C-w>j')
map({'i', 't'}, '˚', '<C-\\><C-N><C-w>k')
map({'i', 't'}, '¬', '<C-\\><C-N><C-w>l')
nmap('˙', '<C-w>h')
nmap('∆', '<C-w>j')
nmap('˚', '<C-w>k')
nmap('¬', '<C-w>l')

vim.cmd 'colorscheme gruvbox'

local signs = {
    Error = '',
    Warn = '',
    Hint = '',
    Info = ''
}

for type, icon in pairs(signs) do
    local hl = 'DiagnosticSign' .. type
    vim.fn.sign_define(hl, {text = icon, texthl = hl, numhl = hl})
end

nmap('<leader>hh', function ()
  if not vim.fn.exists('*synstack*') then
    return
  end
  o = ''
  for k, v in pairs(vim.fn.synstack(vim.fn.line('.'), vim.fn.col('.'))) do
    o = o .. ' ' .. vim.fn.synIDattr(v, 'name')
  end
  print(o)
end
)

pcall(require, 'local')
