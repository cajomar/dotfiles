return {
  {
    'nvim-treesitter/nvim-treesitter',
    version = false,
    event = { 'VeryLazy' },
    build = ':TSUpdate',
    opts = {
      ensure_installed = {
        'hyprlang',
        'markdown',
      },
      highlight = {enable = true},
      indent = {enable = true},
    }
  }
}
