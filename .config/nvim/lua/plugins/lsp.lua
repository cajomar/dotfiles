local function home(path)
  return os.getenv('HOME') .. '/' .. path
end

local function capabilities()
  local cap = vim.lsp.protocol.make_client_capabilities()
  return require'cmp_nvim_lsp'.default_capabilities()
end

local function setup()
  local lspconfig = require 'lspconfig'
  local configs = require 'lspconfig.configs'
  local servers = {
    'bashls',
    'clangd',
    'gopls',
    'pylsp',
    'hls',
    'rust_analyzer',
    'ts_ls',
    'zls',
    'glslls',
    'jqls',
    neocmakelsp = {
      cmd = {'neocmakelsp', '--stdio'},
      filetypes = {'cmake'},
      root_dir = lspconfig.util.find_git_ancestor,
      single_file_support = true,
      init_options = {
        format = {
          enable = true
        }
      }
    }
  }
  local caps = capabilities()
  caps.textDocument.completion.completionItem.snippetSupport = true
  for s, o in pairs(servers) do
    if type(o) == 'string' then
      s = o
    elseif configs[s] == nil then
      configs[s] = { default_config = o }
    end
    lspconfig[s].setup{
      capabilities = caps,
      settings = {
        ['rust-analyzer'] = {
          cargo = {
            allFeatures = true,
            loadOutDirsFromCheck = true,
            runBuildScripts = true,
          },
          -- Add clippy lints for Rust.
          checkOnSave = {
            allFeatures = true,
            command = 'clippy',
            extraArgs = {
              '--',
              '--no-deps',
              '-Dclippy::correctness',
              '-Dclippy::complexity',
              '-Wclippy::perf',
              -- '-Wclippy::pedantic',
            },
          },
        },
      },
    }
  end
end


return {
  {
    'neovim/nvim-lspconfig',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'hrsh7th/cmp-nvim-lsp-signature-help',
    },
    config = setup,
    lazy = false,
  },
  {
    'p00f/clangd_extensions.nvim',
    dependencies = {
      'hrsh7th/cmp-nvim-lsp',
      'neovim/nvim-lspconfig',
    },
    opts = {
      server = {
        capabilities = capabilities(),
      },
    },
  },
};
