return {
  {
    'kyazdani42/nvim-tree.lua',
    keys = {
      {'<leader>tt', '<cmd>NvimTreeToggle<cr>'},
    },
    dependencies = {
      'kyazdani42/nvim-web-devicons',
      'nvim-lua/plenary.nvim',
    },
    opts = {
      sync_root_with_cwd = true,
      respect_buf_cwd = true,
      update_focused_file = {
        enable = true,
        update_root = true
      },
    },
  },

  'mbbill/undotree',

  -- Colorschemes
  {'shaunsingh/solarized.nvim', lazy = true},
  {'ellisonleao/gruvbox.nvim', lazy = true},
  {'jacoborus/tender', lazy = true},
  {'folke/tokyonight.nvim', lazy = true},
  {'catppuccin/nvim', lazy = true},

  -- Languages
  {
    'Glench/Vim-Jinja2-Syntax',
    ft = 'jinja',
  },
  {
    'cespare/vim-toml',
    ft = 'toml',
  },
  {
    'tikhomirov/vim-glsl',
    ft = 'glsl',
  },
  {
    'ziglang/zig.vim',
    ft = 'zig',
  },
  {'bfrg/vim-jq'},
  {
    'lzap/vim-selinux',
    ft = 'selinux',
  },
  {
    'jamespeapen/swayconfig.vim',
    ft = 'swayconfig',
  },
  {
    'evanleck/vim-svelte',
    ft = 'svelte',
  },

  -- Statusline
  {
    'nvim-lualine/lualine.nvim',
    event = 'VeryLazy',
    opts = {
      sections = {
        lualine_b = {'branch', 'diff', 'nvim_diagnostics', 'copilot'},
      }
    }
  },

  'AndreM222/copilot-lualine',

  {
    'akinsho/bufferline.nvim',
    lazy = false,
    dependencies = { 'kyazdani42/nvim-web-devicons' },
    config = true,
  },

  {
    'numToStr/Comment.nvim',
    config = function() 
      require 'Comment'.setup()
    end
  },
  {
    'nvim-telescope/telescope.nvim',
    dependencies = {
      {
        'nvim-telescope/telescope-fzf-native.nvim', 
        build = 'make',
        config = function()
          require 'telescope' .load_extension 'fzf'
        end
      },
      'kyazdani42/nvim-web-devicons',
      'nvim-lua/plenary.nvim',
    },
  },
  {
    'norcalli/nvim-colorizer.lua',
    ft = {
      'css',
      'xml',
    },
    opts = {
      'css',
      'xml',
    },
  },
  {
    'lewis6991/gitsigns.nvim',
    dependencies = {
      "nvim-lua/plenary.nvim",
      {
        "purarue/gitsigns-yadm.nvim",
        opts = {
          shell_timeout_ms = 1000,
        },
      },
    },
    opts = {
      current_line_blame = true,
      current_line_blame_opts = {
        delay = 100,
        ignore_whitespace = true,
      },
      _on_attach_pre = function(_, cb)
        require 'gitsigns-yadm' . yadm_signs(cb)
      end,
      on_attach = function(bufnr)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, opts)
          opts = opts or {}
          opts.buffer = bufnr
          vim.keymap.set(mode, l, r, opts)
        end

        -- Navigation
        map('n', ']c', function()
          if vim.wo.diff then return ']c' end
          vim.schedule(function() gs.next_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        map('n', '[c', function()
          if vim.wo.diff then return '[c' end
          vim.schedule(function() gs.prev_hunk() end)
          return '<Ignore>'
        end, {expr=true})

        -- Actions
        map({'n', 'v'}, '<leader>hs', gs.stage_hunk)
        map({'n', 'v'}, '<leader>hr', gs.reset_hunk)
        map('n', '<leader>hS', gs.stage_buffer)
        map('n', '<leader>hu', gs.undo_stage_hunk)
        map('n', '<leader>hR', gs.reset_buffer)
        map('n', '<leader>hp', gs.preview_hunk)
        map('n', '<leader>hb', function() gs.blame_line{full=true} end)
        map('n', '<leader>tb', gs.toggle_current_line_blame)
        map('n', '<leader>hd', gs.diffthis)
        map('n', '<leader>hD', function() gs.diffthis('~') end)
        map('n', '<leader>td', gs.toggle_deleted)

        -- Text object
        map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
      end,
    },
  },
  {
    'akinsho/toggleterm.nvim',
    opts = {
      open_mapping = [[<c-\>]],
      direction = 'horizontal',
    }
  },
  {
    'ahmedkhalf/project.nvim',
    dependencies = {'nvim-telescope/telescope.nvim'},
    config = function () 
      require 'project_nvim' . setup()
      require 'telescope' . load_extension 'projects'
    end
  },
  {
    '2kabhishek/nerdy.nvim',
    dependencies = {
      'stevearc/dressing.nvim',
      'nvim-telescope/telescope.nvim',
    },
    cmd = 'Nerdy',
  },
  {
    'phelipetls/vim-jqplay',
    cmd = 'Jqplay',
  },
  {
    'zbirenbaum/copilot.lua',
    cmd = 'Copilot',
    event = 'InsertEnter',
    config = true,
  },
  {
    "zbirenbaum/copilot-cmp",
    config = function () require("copilot_cmp").setup() end,
  },
  {
    "yetone/avante.nvim",
    event = "VeryLazy",
    lazy = false,
    version = false, -- set this to "*" if you want to always pull the latest change, false to update on release
    opts = {
      provider = "copilot",
    },
    -- if you want to build from source then do `make BUILD_FROM_SOURCE=true`
    build = "make",
    dependencies = {
      "stevearc/dressing.nvim",
      "nvim-lua/plenary.nvim",
      "MunifTanjim/nui.nvim",
      --- The below dependencies are optional,
      "hrsh7th/nvim-cmp", -- autocompletion for avante commands and mentions
      "nvim-tree/nvim-web-devicons", -- or echasnovski/mini.icons
      "zbirenbaum/copilot.lua", -- for providers='copilot'
      {
        -- support for image pasting
        "HakonHarnes/img-clip.nvim",
        event = "VeryLazy",
        opts = {
          -- recommended settings
          default = {
            embed_image_as_base64 = false,
            prompt_for_file_name = false,
            drag_and_drop = {
              insert_mode = true,
            },
            -- required for Windows users
            use_absolute_path = true,
          },
        },
      },
      {
        -- Make sure to set this up properly if you have lazy=true
        'MeanderingProgrammer/render-markdown.nvim',
        opts = {
          file_types = { "markdown", "Avante" },
        },
        ft = { "markdown", "Avante" },
      },
    },
  }
};
