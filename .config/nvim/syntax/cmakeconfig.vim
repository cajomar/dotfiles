if exists("b:current_syntax")
  finish
endif

runtime! syntax/c.vim
unlet b:current_syntax

syn region	cDefine		start="^\s*\zs\(%:\|#\)\s*\(define\|undef\|cmakedefine01\)\>" skip="\\$" end="$" keepend contains=ALLBUT,@cPreProcGroup,@Spell
