" set runtimepath+="$HOME/.config/nvim/gui"
set guifont=FiraCode\ Nerd\ Font\ Mono:h13
let g:neovide_cursor_vfx_mode = "railgun"
if exists(':GuiTabline')
  GuiTabline 0
endif
if exists(':GuiPopupmenu')
  GuiPopupmenu 0
endif
