# pylint: disable=C0111
c = c  # noqa: F821 pylint: disable=E0602,C0103
config = config  # noqa: F821 pylint: disable=E0602,C0103



config.load_autoconfig()

pass_command = 'spawn --userscript qute-pass --username-target secret --username-pattern "Username: (.+)"'
config.bind('<z><l>', pass_command)
config.bind('<z><u><l>', pass_command + ' --username-only')
config.bind('<z><p><l>', pass_command + ' --password-only')
config.bind('<z><o><l>', pass_command + ' --otp-only')

c.tabs.position = 'right'

c.editor.command = ['nvim-qt', '--nofork', '{file}', '--', '-c', 'normal {line}G{collumn0}l']

# c.content.javascript.enabled = False

c.url.searchengines = {
        "DEFAULT": "https://duckduckgo.com/?q={}",
        "g": "https://google.com/search?hl=en&q={}",
        }
