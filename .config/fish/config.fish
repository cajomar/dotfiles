if status is-interactive
    # Commands to run in interactive sessions can go here
end

set PATH $PATH $HOME/.bin $HOME/.local/bin $HOME/.cargo/bin $HOME/go/bin

alias df='df -h'
# alias diff='diff --color --unified'
alias free='free -m'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias ip='ip --color=auto'
alias ls='ls --color=auto'
alias pysr='python3 -m http.server'
alias lg='lazygit'

export EDITOR='nvim'
export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=10000
export SAVEHIST=10000
export BROWSER='firefox'
export CMAKE_EXPORT_COMPILE_COMMANDS='ON'
# Force color output for GCC and Clang with Ninja
export CFLAGS='-fdiagnostics-color=always'
export CXXFLAGS='-fdiagnostics-color=always'
export CPPFLAGS='-fdiagnostics-color=always'
export RIPGREP_CONFIG_PATH="$HOME/.config/ripgreprc"
export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

if type -q jj
  jj --version >/dev/null && jj util completion fish | source
end
